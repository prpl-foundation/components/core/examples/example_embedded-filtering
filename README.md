# Example Third Party Interception plug-in


This is an Ambiorix plug-in for an example third party interception application.
It is meant to be used with the Packet Interception plugin

It will start listening to the /tmp/dns_intercept UNIX socket.
It will parse the received packets and print some information about them.
It will send an accept verdict for all the received packets.

The most interesting part of the component can be found in src/packet-interception.c
Here the components connects to the socket and communicates with the Packet Interception component.
