# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.2 - 2024-01-22(10:53:50 +0000)

### Changes

- [Example EmbeddedFiltering] update to new libimtp API

### Other

- Opensource component

## Release v0.1.1 - 2022-12-19(15:58:57 +0000)

### Other

- [PacketInterception] example component does not start

## Release v0.1.0 - 2022-12-06(13:27:43 +0000)

