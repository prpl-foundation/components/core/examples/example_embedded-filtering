#!/bin/sh

name="ex-embeddedfiltering"

case $1 in
    start|boot)
        ${name} -D /etc/amx/ex-embeddedfiltering/embeddedfiltering.odl
        ;;
    stop)
        kill 'cat /var/run/embeddedfiltering.pid'
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|restart]"
        ;;
esac
