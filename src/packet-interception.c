/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>

#include "packet-interception.h"
#include <imtp/imtp_connection.h>
#include <netlink-utils/nfqueue.h>
#include <packet-interception/packet.h>

#include <yajl/yajl_gen.h>
#include <amxj/amxj_variant.h>

#define ME "packet-interception"
#define SOCKET "/tmp/dns_intercept"

static imtp_connection_t* pi_conn = NULL;

static void packet_reply(imtp_connection_t* conn, packet_id_t* packet) {
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* packet_tlv = NULL;
    imtp_tlv_t* verdict_tlv = NULL;
    int retval = -1;
    packet_verdict_t verdict;

    verdict.verdict = NF_ACCEPT;
    verdict.mark = 1;
    verdict.mark_mask = 0;

    retval = imtp_frame_new(&frame);
    when_failed(retval, exit);

    retval = imtp_tlv_new(&packet_tlv, imtp_tlv_type_packet_id, sizeof(packet_id_t), packet, 0, IMTP_TLV_COPY);
    when_failed(retval, exit);

    retval = imtp_tlv_new(&verdict_tlv, imtp_tlv_type_packet_verdict, sizeof(packet_verdict_t), &verdict, 0, IMTP_TLV_COPY);
    when_failed(retval, exit);

    retval = imtp_frame_tlv_add(frame, packet_tlv);
    when_failed(retval, exit);

    retval = imtp_frame_tlv_add(frame, verdict_tlv);
    when_failed(retval, exit);

    retval = imtp_connection_write_frame(conn, frame);
    when_failed(retval, exit);

exit:
    imtp_frame_delete(&frame);
    return;
}

static void packet_cb(UNUSED int fd, UNUSED void* data) {
    imtp_frame_t* frame = NULL;
    const imtp_tlv_t* packet_tlv = NULL;
    const imtp_tlv_t* packet_meta_tlv = NULL;
    const imtp_tlv_t* packet_payload_tlv = NULL;
    const imtp_tlv_t* json_tlv = NULL;
    imtp_connection_t* conn = (imtp_connection_t* ) data;
    packet_id_t packet;
    packet_data_t packet_data;
    amxc_var_t json_var;
    char* json_buffer = NULL;
    int retval = 0;

    retval = imtp_connection_read_frame(conn, &frame);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not read data from socket");
        imtp_connection_delete(&conn);
        goto exit;
    }

    if(retval > 0) {
        SAH_TRACEZ_INFO(ME, "Not all data was read, more data still coming");
        goto exit;
    }

    packet_tlv = imtp_frame_get_first_tlv(frame, imtp_tlv_type_packet_id);
    if(!packet_tlv) {
        SAH_TRACEZ_ERROR(ME, "Data did not not contain Packet");
        goto exit;
    }

    memcpy(&packet, (uint8_t*) packet_tlv->value + packet_tlv->offset, packet_tlv->length);
    SAH_TRACEZ_INFO(ME, "Packet group [%d] ID [%d]", packet.group, packet.id);

    json_tlv = imtp_frame_get_first_tlv(frame, imtp_tlv_type_json);
    if(json_tlv) {
        json_buffer = (char*) calloc(1, json_tlv->length + 1);
        when_null(json_buffer, exit);
        memcpy(json_buffer, (uint8_t*) json_tlv->value + json_tlv->offset, json_tlv->length);

        SAH_TRACEZ_INFO(ME, "[%s]", json_buffer);

        amxc_var_init(&json_var);

        amxc_var_set(jstring_t, &json_var, json_buffer);
        amxc_var_cast(&json_var, AMXC_VAR_ID_ANY);

        amxc_var_dump(&json_var, STDOUT_FILENO);
    } else {
        packet_meta_tlv = imtp_frame_get_first_tlv(frame, imtp_tlv_type_packet_meta);
        packet_payload_tlv = imtp_frame_get_first_tlv(frame, imtp_tlv_type_packet_payload);
        if(!packet_meta_tlv || !packet_payload_tlv) {
            SAH_TRACEZ_ERROR(ME, "No packet payload found");
            goto exit;
        }

        memcpy(&packet_data, (uint8_t*) packet_meta_tlv->value + packet_meta_tlv->offset, packet_meta_tlv->length);
        packet_data.payload = calloc(1, packet_payload_tlv->length);
        memcpy(packet_data.payload, (uint8_t*) packet_payload_tlv->value + packet_payload_tlv->offset, packet_payload_tlv->length);

        SAH_TRACEZ_INFO(ME, "Packet Family [%d]", packet_data.family);
        SAH_TRACEZ_INFO(ME, "Packet SKB mark [%d]", packet_data.mark);

        free(packet_data.payload);
    }

    packet_reply(conn, &packet);

exit:
    free(json_buffer);
    imtp_frame_delete(&frame);
    return;
}

bool packet_interception_initialize(void) {
    int retval = -1;
    bool ret = false;

    retval = imtp_connection_connect(&pi_conn, NULL, SOCKET);
    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "Could not connect to UNIX domain socket [%s]", SOCKET);
        goto exit;
    }

    retval = amxo_connection_add(ef_get_parser(), imtp_connection_get_fd(pi_conn), packet_cb, NULL, AMXO_CUSTOM, pi_conn);
    if(retval != 0) {
        SAH_TRACEZ_INFO(ME, "Could not add parser to connection");
        imtp_connection_delete(&pi_conn);
        goto exit;
    }

    ret = true;

exit:
    return ret;
}

void packet_interception_cleanup(void) {
    imtp_connection_delete(&pi_conn);
}
